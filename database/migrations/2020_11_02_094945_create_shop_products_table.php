<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateShopProductsTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create('shop_products', function (Blueprint $table) {
			$table->bigIncrements('id');
			$table->string('name');
			$table->string('categories')->nullable();
			$table->string('origin')->nullable();
			$table->string('brand')->nullable();
			$table->string('model')->nullable();
			$table->string('color')->nullable();
			$table->string('part')->nullable();
			$table->string('material')->nullable();
			$table->string('size')->nullable();
			$table->string('width')->nullable();
			$table->string('height')->nullable();
			$table->string('price')->nullable();
			$table->string('reserves')->nullable();
			$table->string('shipping_fee_payment')->nullable();
			$table->text('description')->nullable();
			$table->string('reference')->nullable();
			$table->string('image')->nullable();
			$table->string('link');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists('shop_products');
	}
}
