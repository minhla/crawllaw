<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateShopCategoriesTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create('shop_categories', function (Blueprint $table) {
			$table->bigIncrements('id');
			$table->integer('parent_id')->default(0)->nullable();
			$table->integer('level')->default(1)->nullable()->comment('Values: 1,2,3');
			$table->string('name')->nullable();
			$table->string('path')->nullable();
			$table->string('slug')->nullable();
			$table->string('link');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::dropIfExists('shop_categories');
	}
}
