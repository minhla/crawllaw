<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Law extends Model
{
    protected $fillable = ['name', 'slug', 'content'];

    public function content() {
        return $this->hasOne('App\Content');
    }
}
