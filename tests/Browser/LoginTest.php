<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class LoginTest extends DuskTestCase
{
    /**
     * A Dusk test example.
     *
     * @return void
     */
    public function testExample()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('https://khoinghiep.thuvienphapluat.vn/phap-ly-trong-qua-trinh-hoat-dong-trong-cong-ty-co-phan/48.html')
                    ->assertSee('Laravel');
        });
    }
}
