<?php

namespace App\Console\Commands;

use App\Content;
use App\Law;
use App\Template;
use Goutte\Client;
use Illuminate\Console\Command;
use Symfony\Component\HttpClient\HttpClient;

class CrawlLawCommad extends Command {
	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'command:crawl-law {status}';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Crawl Law Content';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct() {
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle() {
		$username = "shorthair1996@gmail.com";
		$pass = "hds2020";

		$domain = "https://khoinghiep.thuvienphapluat.vn";
		$loginLink = $domain . "/dang-nhap.html";

		$client = new Client(HttpClient::create([
			'base_uri' => $domain,
			'headers' => [
				'User-Agent' => "Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2272.101 Safari/537.36",
			],
		]));

		$crawler = $client->request('GET', $loginLink);

		// select the form and fill in some values
		$form = $crawler->selectButton('Đăng nhập')->form();
		$form['Email'] = $username;
		$form['Password'] = $pass;

		// submit that form
		$crawler = $client->submit($form);

		$status = $this->argument('status');
		if ($status == 0) {
			$theUrl = 'https://khoinghiep.thuvienphapluat.vn/cong-viec-phap-ly.html';
			$crawler = $client->request('GET', $theUrl);
			$crawler->filter('table.tableCate > tbody > tr a')->each(function ($node) use ($client) {
				$href = $node->getNode(0)->getAttribute('href');
				if (Law::where('slug', $href)->first() == NULL) {
					echo $href . "\n";
					$crawler = $client->request('GET', 'https://khoinghiep.thuvienphapluat.vn' . $href);

					// Main Content
					$name = $crawler->filter('div#noi-dung-cong-viec > h1')->text();
					$description = $crawler->filter('div.CategoryContent')->html();

					$lawParent = Law::create([
						'name' => $name,
						'slug' => $href,
						'content' => base64_encode($description),
					]);

					$crawler->filter('div.CategoryAttachments > ul > li a.btn-success')->each(function ($el) use ($lawParent, $client) {
						$href = $el->getNode(0)->getAttribute('href');
						if (trim($href) != "") {
							$exist = Template::where("path", $href)->first();
							$name = $el->text();
							if ($exist == NULL) {
								$crawler = $client->request('GET', 'https://khoinghiep.thuvienphapluat.vn' . $href);

								echo 'main url -> template url -> template download url: ' . $href . "\n";
								$crawler->filter('.mb5.mt10 > button')->each(function ($button) use ($lawParent, $name, $client, $href) {
									if ($button->text() == 'Tải biểu mẫu gốc') {
										$onclick = $button->getNode(0)->getAttribute('onclick');
										preg_match('/\("([^"]+)"\)/m', $onclick, $match);
										$templateId = end($match);
										$templateDownloadLink = 'https://khoinghiep.thuvienphapluat.vn/tai-bieu-mau.html?id=' . $templateId;

										$cookieJar = $client->getCookieJar();

										if (!\Storage::exists('public/files/cookie.txt')) {
											\Storage::put('public/files/cookie.txt', $cookieJar->all());
										}

										if (!\Storage::exists('public/files/' . $templateId . ".doc")) {
											$httpclient = HttpClient::create([
												'base_uri' => "https://khoinghiep.thuvienphapluat.vn",
												'headers' => [
													'User-Agent' => "Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2272.101 Safari/537.36",
													'Cookie' => $cookieJar->all(),
												],
											]);

											$response = $httpclient->request('GET', $templateDownloadLink);

											\Storage::put('public/files/' . $templateId . ".doc", $response->getContent());
										}

										Template::create([
											'name' => $name,
											'law_id' => $lawParent->id,
											'filename' => $templateId . ".doc",
											'path' => $href,
										]);
									}
								});
							} else {
								Template::create([
									'name' => $name,
									'law_id' => $lawParent->id,
									'filename' => $exist->filename,
									'path' => $exist->path,
								]);
							}
						}
					});

					$crawler->filter('div.CategoryLaws > ul > li a')->each(function ($li) use ($lawParent, $client) {
						$href = $li->getNode(0)->getAttribute('href');
						$exist = Content::where("slug", $href)->first();
						$name = $li->text();
						if ($exist == NULL) {
							$crawler = $client->request('GET', $href);
							echo 'content download url: ' . $href . "\n";

							$checkname = $crawler->filter("h1");
							if ($checkname->count() == 0) {
								logger($crawler->html());
								dd("Finish: " . $lawParent->id);
							} else {
								$temp = $checkname->text();
								if (strlen($temp) > 120) {
									$temp = substr($temp, 0, 120) . "..";
								}
								$namefile = $temp . ".doc";
							}

							if (!\Storage::exists('public/files/' . $namefile)) {
								$cookieJar = $client->getCookieJar();
								$httpclient = HttpClient::create([
									'base_uri' => "https://thuvienphapluat.vn",
									'headers' => [
										'User-Agent' => "Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2272.101 Safari/537.36",
										'Cookie' => $cookieJar->all(),
									],
								]);

								$response = $httpclient->request('GET', $crawler->filter("#Content_ThongTinVB_vietnameseHyperLink")->getNode(0)->getAttribute('href'));

								\Storage::put('public/files/' . $namefile, $response->getContent());
							}

							$content = $crawler->filter('#divContentDoc')->html();
							Content::create([
								'name' => $name,
								'content' => $content,
								'slug' => $href,
								'law_id' => $lawParent->id,
								"file" => base64_encode($namefile),
							]);
						} else {
							Content::create([
								'name' => $name,
								'content' => $exist->content,
								'slug' => $exist->slug,
								'law_id' => $lawParent->id,
								"file" => $exist->file,
							]);
						}
					});
				}
			});
		} else {
			Law::find($status)->delete();
			Content::where("law_id", $status)->delete();
			Template::where("law_id", $status)->delete();
		}
	}
}
