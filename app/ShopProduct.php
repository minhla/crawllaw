<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ShopProduct extends Model {
	protected $fillable = ['name', 'categories', 'origin', 'brand', 'model', 'color', 'part', 'material', 'size', 'width', 'height', 'price', 'reserves', 'shipping_fee_payment', 'description', 'reference', 'image', 'link'];
}