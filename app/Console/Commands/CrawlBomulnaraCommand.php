<?php

namespace App\Console\Commands;

use App\ShopCategory;
use App\ShopProduct;
use Goutte\Client;
use Illuminate\Console\Command;
use Symfony\Component\HttpClient\HttpClient;

class CrawlBomulnaraCommand extends Command {
	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'command:crawl-shop {status}';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Crawl http://www.bomulnara.net/ Content';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct() {
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle() {
		$domain = "http://www.bomulnara.net/";
		$loginLink = $domain . "/dang-nhap.html";

		$client = new Client(HttpClient::create([
			'base_uri' => $domain,
			'headers' => [
				'User-Agent' => "Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2272.101 Safari/537.36",
			],
		]));

		$status = $this->argument('status');
		if ($status == 0) {
			$ca = ['01', '02', '31', '04', '51', '03', '61', '12', '81', '91', 'a1', 'b1'];
			$baseShopUrl = 'http://www.bomulnara.net/shop/list.php?ca_id=';

			foreach ($ca as $key) {
				$theUrl = $baseShopUrl . $key;
				# Create category level 1
				ShopCategory::create();
				$crawler = $client->request('GET', $theUrl);
				// Child cate
				$crawler->filter('.list-category ul.nav-tabs > li a')->each(function ($node) use ($client) {
					# Create category level 2
					# Check cate exist and add parent
					$href = $el->getNode(0)->getAttribute('href');
					if (trim($href) != "") {
						$crawler = $client->request('GET', $href);
						// Child cate
						$crawler->filter('.list-category ul.nav-tabs > li')->each(function ($node) use ($client) {
							# Create category level 3
							$href = $el->getNode(0)->getAttribute('href');
							if (trim($href) != "") {
								$crawler = $client->request('GET', $href);
								$crawler->filter('.list-category ul.nav-tabs > li')->each(function ($node) use ($client) {
									# Create product
									# Creare link category product by breadcum
								});
							}
						});
					}
				});
			}
		} else {
			ShopProduct::find($status)->delete();
		}
	}
}
