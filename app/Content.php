<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Content extends Model
{
    protected $fillable = ['content', 'name', 'law_id', 'slug', 'file'];

    public function law() {
        return $this->belongsTo('App\Law');
    }
}
