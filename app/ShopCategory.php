<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ShopCategory extends Model {
	protected $fillable = ['parent_id', 'name', 'link', 'level'];

	public function parent() {
		return $this->belongsTo('App\ShopCategory', 'parent_id');
	}
}
